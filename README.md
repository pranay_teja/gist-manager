## Web app to manage GitHub Gists

[Visit React App](https://gistman.now.sh/ "GistMan")

[Visit Angular App](https://gistmanager.now.sh/ "GistManager")

[View Project Status](https://app.gitkraken.com/glo/board/XlPsSXV7egARLgJ9 "GistManager Glo Board")

[View Timeline](https://timelines.gitkraken.com/timeline/1c808740b3b54f419a160c3fa8b9f13d "GistManager Timeline")